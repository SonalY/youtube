from django import forms


class CommentForm(forms.Form):
    comment = forms.CharField()


class UploadForm(forms.Form):
    title = forms.CharField(max_length=200)
    description = forms.CharField(max_length=450)
    categories = forms.CharField(max_length=200)
    video = forms.FileField()
    video_poster = forms.FileField()

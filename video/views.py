from rest_framework.renderers import TemplateHTMLRenderer
from rest_framework import views

from django.urls import reverse
from django.shortcuts import render
from django.utils.dateparse import parse_datetime
from django.http import HttpResponseRedirect, HttpResponse
from django.contrib.auth.decorators import login_required
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

from .serializers import VideoSerializer
from .forms import CommentForm, UploadForm
from .interactors import video_interactor, comment_interactor, category_interactor, like_interactor, dislike_interactor


class VideoViewSet(views.APIView):
    renderer_classes = [TemplateHTMLRenderer]
    serializer_class = VideoSerializer

    def get(self, request):
        videos = video_interactor.get_all()
        serializer = VideoSerializer(videos, many=True)
        videos = list(serializer.data)
        page = request.GET.get('page', 1)
        paginator = Paginator(videos, 10)

        for video in videos:
            date = video['uploaded_at']
            video['uploaded_at'] = parse_datetime(date).date()

        try:
            videos = paginator.page(page)
        except PageNotAnInteger:
            videos = paginator.page(1)
        except EmptyPage:
            videos = paginator.page(paginator.num_pages)

        return render(request, template_name='video/home.html',
                      context={'videos': videos,
                               'user': request.user.username})


@login_required
def get_video(request, id):
    video = video_interactor._get(id=id)
    categories = category_interactor.filter_by_video(video_id=video.id)
    comments = comment_interactor.filter_by_video(video_id=video.id)
    likes = like_interactor.get_count(video_id=video.id)
    dislikes = dislike_interactor.get_count(video_id=video.id)
    return render(request=request, template_name='video/video.html', context={
                                                'video': video,
                                                'categories': categories,
                                                'user': request.user,
                                                'likes': likes,
                                                'dislikes': dislikes,
                                                'comments': comments},
                  status=200)


@login_required
def upload(request):
    if request.method == 'POST':
        form = UploadForm(request.POST, request.FILES)

        if form.is_valid():
            title = form.cleaned_data['title']
            description = form.cleaned_data['description']
            raw_categories = form.cleaned_data['categories']
            video = form.cleaned_data['video']
            video_poster = form.cleaned_data['video_poster']
            categories = [x.strip() for x in raw_categories.split(',')]

            video_obj = video_interactor.create_video(
                title=title, description=description, video=video,
                video_poster=video_poster, uploaded_by_id=request.user.id)

            for category in categories:
                category_obj = category_interactor.get_or_create(name=category)
                category_interactor.create_video_category(
                    category_id=category_obj[0].id, video_id=video_obj.id)
                category_obj[0].video.add(video_obj)

            response = HttpResponseRedirect(reverse('home'))
        else:
            form = UploadForm()
            response = HttpResponseRedirect(reverse('400'))
    else:
        form = UploadForm()
        response = render(request=request, template_name='video/upload.html',
                          context={'form': form})
    return response


@login_required
def user_reaction(request, id):
    video = video_interactor._get(id=id)
    form = CommentForm(request.POST)

    if request.method == 'POST':
        likes = like_interactor.get_likes_for_user(
            user_id=request.user.id, video_id=video.id)
        dislikes = dislike_interactor.get_dislikes_for_user(
            user_id=request.user.id, video_id=video.id)

        if request.POST.get('like'):
            if not likes:
                like_interactor.create(user_id=request.user.id,
                                       video_id=video.id)
                if dislikes:
                    dislikes.delete()
            else:
                likes.delete()

        elif request.POST.get('dislike'):
            if not dislikes:
                dislike_interactor.create(user_id=request.user.id,
                                          video_id=video.id)
                if likes:
                    likes.delete()
            else:
                dislikes.delete()
        elif form.is_valid():
            comment = form.cleaned_data['comment']
            comment_interactor.create(text=comment,
                                      added_by=request.user.id,
                                      video_id=video.id)
    else:
        form = CommentForm()

    return HttpResponseRedirect(reverse('video', args=(id,)))

from rest_framework import serializers
from .models import Video


class VideoSerializer(serializers.ModelSerializer):
    uploaded_by_name = serializers.CharField(
        source='uploaded_by.username', read_only=True)

    class Meta:
        model = Video
        fields = ('title', 'description', 'uploaded_by_name',
                  'uploaded_at', 'video_poster', 'id')

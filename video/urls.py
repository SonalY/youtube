from django.urls import path
# from django.views.generic import TemplateView

from .views import VideoViewSet, upload, get_video, user_reaction

urlpatterns = [
    path('', VideoViewSet.as_view(), name='home'),
    path('<int:id>', get_video, name='video'),
    path('new', upload, name='upload'),
    path('reaction/<int:id>', user_reaction, name='user-reaction'),
]

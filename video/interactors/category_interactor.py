from ..models import Category, CategoryVideo


def filter_by_video(video_id):
    return Category.objects.filter(video=video_id)


def get_or_create(name):
    return Category.objects.get_or_create(name=name)


def create_video_category(category_id, video_id):
    return CategoryVideo.objects.create(category_id=category_id,
                                        video_id=video_id)

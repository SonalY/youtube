from ..models import Dislike


def get_count(video_id):
    return Dislike.objects.filter(video_id=video_id).count()


def get_dislikes_for_user(user_id, video_id):
    return Dislike.objects.filter(user_id=user_id, video_id=video_id)


def create(user_id, video_id):
    Dislike.objects.create(user_id=user_id, video_id=video_id)

from ..models import Video
from django.shortcuts import get_object_or_404


def _get(id):
    return get_object_or_404(Video.objects.select_related('uploaded_by'), pk=id)


def get_all():
    return Video.objects.all().select_related('uploaded_by')


def create_video(title, description, video, video_poster, uploaded_by_id):
    return Video.objects.create(title=title,
                                description=description,
                                video=video,
                                video_poster=video_poster,
                                uploaded_by_id=uploaded_by_id)

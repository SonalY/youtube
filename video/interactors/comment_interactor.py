from ..models import Comment


def filter_by_video(video_id):
    return Comment.objects.filter(video_id=video_id).select_related('added_by')


def create(text, added_by, video_id):
    return Comment.objects.create(text=text,
                                  added_by_id=added_by,
                                  video_id=video_id)

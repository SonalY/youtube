from ..models import Like


def get_count(video_id):
    return Like.objects.filter(video_id=video_id).count()


def get_likes_for_user(user_id, video_id):
    return Like.objects.filter(user_id=user_id, video_id=video_id)


def create(user_id, video_id):
    Like.objects.create(user_id=user_id, video_id=video_id)

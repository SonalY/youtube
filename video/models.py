from django.db import models
from user.models import User


class Video(models.Model):
    title = models.CharField(max_length=200)
    description = models.CharField(max_length=450, blank=True)
    uploaded_by = models.ForeignKey(to=User, on_delete=models.CASCADE)
    uploaded_at = models.DateTimeField(auto_now_add=True)
    video_poster = models.ImageField(upload_to='video_posters/')
    video = models.FileField(upload_to='videos/')

    def __str__(self):
        return self.title

    class Meta:
        ordering = ['-uploaded_at']


class Category(models.Model):
    name = models.CharField(max_length=50)
    video = models.ManyToManyField(Video)

    def __str__(self):
        return self.name


class Comment(models.Model):
    text = models.CharField(max_length=450)
    added_at = models.DateTimeField(auto_now_add=True)
    added_by = models.ForeignKey(to=User, on_delete=models.CASCADE)
    video = models.ForeignKey(to=Video, on_delete=models.CASCADE)


class Like(models.Model):
    user = models.ForeignKey(to=User, on_delete=models.CASCADE)
    video = models.ForeignKey(to=Video, on_delete=models.CASCADE)

    class Meta:
        unique_together = ['user', 'video']


class Dislike(models.Model):
    user = models.ForeignKey(to=User, on_delete=models.CASCADE)
    video = models.ForeignKey(to=Video, on_delete=models.CASCADE)

    class Meta:
        unique_together = ['user', 'video']


class CategoryVideo(models.Model):
    category = models.ForeignKey(to=Category, on_delete=models.CASCADE)
    video = models.ForeignKey(to=Video, on_delete=models.CASCADE)

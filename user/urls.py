from django.urls import path
from user import views

urlpatterns = [
    path('login', views.user_login, name='user-login'),
    path('register', views.register, name='register'),
    path('logout', views.user_logout, name='logout')
]

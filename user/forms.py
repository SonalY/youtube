from django import forms
from .models import User


class UserForm(forms.ModelForm):
    username = forms.CharField(max_length=50, required=True)
    password = forms.CharField(widget=forms.PasswordInput())
    email = forms.EmailField(max_length=50)

    class Meta:
        model = User
        fields = ('username', 'password', 'email')


class LoginForm(forms.Form):
    username = forms.CharField(max_length=50, required=True)
    password = forms.CharField(widget=forms.PasswordInput(), required=True)

from django.shortcuts import render, redirect
from django.contrib.auth import login, authenticate, logout
from django.contrib.auth.decorators import login_required

from .forms import UserForm, LoginForm


def register(request):
    if request.method == 'POST':
        form = UserForm(request.POST)
        if form.is_valid():
            user = form.save()
            user.set_password(user.password)
            user.save()
            login(request, user)
            return redirect('/videos')
        else:
            message = "The details you entered might be wrong, please try again."
            form = UserForm()
            return render(request, 'user_auth/register.html', {'form': form,
                                                               'message': message}, status=400)
    else:
        form = UserForm()
    return render(request, 'user_auth/register.html', {'form': form})


def user_login(request):
    if request.method == 'POST':
        form = LoginForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']
            user = authenticate(username=username, password=password)
            if user:
                login(request, user)
                return redirect('/videos')
        else:
            form = LoginForm()
            message = "The details you entered might be wrong, please try again."
            return render(request, 'user_auth/login.html', {'message': message}, status=400)
    else:
        form = LoginForm()
    return render(request, 'user_auth/login.html')


@login_required
def user_logout(request):
    logout(request)

    return redirect('/')
